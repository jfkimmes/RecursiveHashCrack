# Recursive Hash Cracking

Crack recursively hashed secrets of the form `Md5(Md5(Md5($secret)))` in parallel.

## Disclaimer
- Naive (though parallel) implementation
- Only does md5 at the moment
- Bring your own wordlist

## Performance
- 11th Gen Intel Core i5-1135G7 x8
- 1 Million words ([000000-999999 Wordlist](https://github.com/danielmiessler/SecLists/blob/master/Fuzzing/6-digits-000000-999999.txt))

| Rounds (md5) | Time |
|--------------|------|
| 1            | 0.1s |
| 100          | 1.5s |
| 1000         | 17s  |
| 10000        | 4m   |

