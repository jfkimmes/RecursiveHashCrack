use std::fs::File;
use std::io::{BufReader, BufRead};
use std::process::exit;
use std::env;

use md5::Digest;

use rayon::prelude::*;


fn main() {
    const CHUNK_SIZE: usize = 1024 * 1024 * 1024 * 2;

    let args: Vec<String> = env::args().collect();
    if args.len() != 5 {
        println!("Wrong usage.");
        println!("-- <path to wordlist> <target hash digest> <number of rounds> <hash algorithm>");
        println!("-- e.g., ./wordlist.txt abcdef1234567890 1000 md5");
        exit(1)
    }

    // parse command line args
    let wordlist_path = &args[1];
    let target_hash_hex = hex::decode(&args[2].as_bytes()).expect("Could not decode target hash.");
    let rounds = u32::from_str_radix(&args[3], 10).expect("Could not convert rounds to int.");
    let _algo = &args[4]; // TODO: support more than md5

    // read file in chunks
    let f = File::open(wordlist_path).expect("Could not open wordlist.");
    let mut reader = BufReader::with_capacity(CHUNK_SIZE, f);
    loop {
        // FIXME: this potentially splits and breaks a word at every chunk
        let chunk = reader.fill_buf().expect("Could not read next chunk.");
        let bytes_read = chunk.len();

        println!("Processing {:?} bytes.", bytes_read);
        if bytes_read == 0 {
            println!("Processed all words in wordlist.");
            exit(1);
        }

        let words = std::str::from_utf8(chunk).expect("Could not convert chunk to words.");

        words.par_lines().for_each(| word: &str | {
            let mut hasher = md5::Md5::new(); 

            let word_bytes: &[u8] = word.as_bytes();
            let mut intermediate_hash_buffer = [0u8; 16];

            hasher.update(word_bytes);
            intermediate_hash_buffer.copy_from_slice(&hasher.finalize_reset());

            for _ in 1..rounds {
                hasher.update(intermediate_hash_buffer);
                intermediate_hash_buffer.copy_from_slice(&hasher.finalize_reset());
            }

            if target_hash_hex == intermediate_hash_buffer {
                println!("Found candidate!");
                println!("Canidate: {:?}", word);
                exit(0)
            }
        });

        reader.consume(bytes_read);
    }
}
